package com.training.tradingapp.converters;

import com.training.tradingapp.enums.Status;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.stream.Stream;

@Converter
public class StatusConverter implements AttributeConverter<Status, String> {


    @Override
    public String convertToDatabaseColumn(Status status) {
        if(status == null) return null;
        return status.getShortName();
    }

    @Override
    public Status convertToEntityAttribute(String code) {
        if(code == null) return null;
        return Stream.of(Status.values())
                .filter(c -> c.getShortName().equals(code))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
    }
}
