package com.training.tradingapp.converters;

import com.training.tradingapp.enums.BuySell;
import com.training.tradingapp.enums.Status;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.stream.Stream;

@Converter
public class BuySellConverter implements AttributeConverter<BuySell, String> {

    @Override
    public String convertToDatabaseColumn(BuySell buySell) {
        if(buySell == null) return null;
        return buySell.getShortName();
    }

    @Override
    public BuySell convertToEntityAttribute(String s) {
        if(s == null) return  null;
        return Stream.of(BuySell.values())
                .filter(c -> c.getShortName().equals(s))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);

    }
}
