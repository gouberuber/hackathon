package com.training.tradingapp.entities;

import com.training.tradingapp.converters.BuySellConverter;
import com.training.tradingapp.converters.StatusConverter;
import com.training.tradingapp.enums.BuySell;
import com.training.tradingapp.enums.Status;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="order_activity")
public class Order {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column
    private Long id;

    @Column
    private String stockTicker;

    @Column
    private Double price;

    @Column
    private Integer volume;

    @Column
    @Convert(converter = BuySellConverter.class)
    private BuySell buyOrSell;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date timestamp;

    @Column
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column
    @Convert(converter = StatusConverter.class)
    private Status status;

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", stockTicker='" + stockTicker + '\'' +
                ", price=" + price +
                ", volume=" + volume +
                ", buyOrSell=" + buyOrSell +
                ", timestamp=" + timestamp +
                ", name='" + name + '\'' +
                ", status=" + status +
                '}';
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStockTicker() {
        return stockTicker;
    }

    public void setStockTicker(String stockTicker) {
        this.stockTicker = stockTicker;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getVolume() {
        return volume;
    }

    public void setVolume(Integer volume) {
        this.volume = volume;
    }

    public BuySell getBuyOrSell() {
        return buyOrSell;
    }

    public void setBuyOrSell(BuySell buyOrSell) {
        this.buyOrSell = buyOrSell;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }
}



