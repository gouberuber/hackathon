package com.training.tradingapp.service;

import com.training.tradingapp.entities.Order;
import com.training.tradingapp.repository.OrderActivityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderActivityService {

    @Autowired
    OrderActivityRepository orderActivityRepository;

    public List<Order> findAll() {
        return orderActivityRepository.findAll();
    }

    public Order findById(Long id) {
        return orderActivityRepository.findById(id).get();
    }

    public Order create(Order order){
        return orderActivityRepository.save(order);
    }

    public Order update(Order order) {
        orderActivityRepository.findById(order.getId()).get();

        return orderActivityRepository.save(order);
    }

    public void delete(long id) {
        orderActivityRepository.deleteById(id);
    }

}
