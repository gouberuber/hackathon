package com.training.tradingapp.service;

import com.training.tradingapp.entities.Trade;
import com.training.tradingapp.repository.TradeActivityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TradeActivityService {

    @Autowired
    TradeActivityRepository tradeActivityRepository;

    public List<Trade> findAll() { return tradeActivityRepository.findAll(); };
    public Trade findById(Long id) { return tradeActivityRepository.findById(id).get(); };
    public Trade create(Trade trade) { return tradeActivityRepository.save(trade); };
    public Trade update(Trade trade){
        tradeActivityRepository.findById(trade.getId()).get();
        return tradeActivityRepository.save(trade);
    }
    public void delete(long id) { tradeActivityRepository.deleteById(id); };


}
