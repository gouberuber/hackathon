package com.training.tradingapp.enums;

public enum Status {
    COMPLETE("C"), PENDING("P"), FAILED("F"), INIT("I");

    public String getShortName() {
        return shortName;
    }

    private String shortName;

    Status(String shortname){
        this.shortName = shortname;
    }
}
