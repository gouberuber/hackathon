package com.training.tradingapp.enums;

public enum BuySell {
    BUY("B"), SELL("S");

    public String getShortName() {
        return shortName;
    }

    private String shortName;
    BuySell(String shortname){
        this.shortName = shortname;
    }


}