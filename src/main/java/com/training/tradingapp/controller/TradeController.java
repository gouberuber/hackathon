package com.training.tradingapp.controller;

import com.training.tradingapp.entities.Trade;
import com.training.tradingapp.service.TradeActivityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/trade")
@CrossOrigin("*")
public class TradeController {

    private static final Logger LOG = LoggerFactory.getLogger(OrderController.class);

    @Autowired
    private TradeActivityService tradeActivityService;

    @GetMapping
    public List<Trade> findAll() { return tradeActivityService.findAll(); }

    @GetMapping("/{id}")
    public ResponseEntity<Trade> findByID(@PathVariable long id){
        try{
            return new ResponseEntity<>(tradeActivityService.findById(id), HttpStatus.OK);
        }catch(NoSuchElementException ex){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public ResponseEntity<Trade> create(@RequestBody Trade trade){
        return new ResponseEntity<>(tradeActivityService.create(trade), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<Trade> update(@RequestBody Trade trade){
        try{
            return new ResponseEntity<>(tradeActivityService.update(trade), HttpStatus.OK);
        }catch(NoSuchElementException ex){
            LOG.debug("Update for unknown id: [" + trade + "] - trade");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable long id){
        try{
            tradeActivityService.delete(id);
        }catch(EmptyResultDataAccessException ex){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

}
