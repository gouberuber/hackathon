package com.training.tradingapp.controller;

import com.training.tradingapp.entities.Order;
import com.training.tradingapp.service.OrderActivityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@RestController
@RequestMapping("/order")
@CrossOrigin("*")
public class OrderController {

    private static final Logger LOG = LoggerFactory.getLogger(OrderController.class);

    @Autowired
    private OrderActivityService orderActivityService;


    @GetMapping
    public List<Order> findAll() {
        return orderActivityService.findAll();
    }
    @GetMapping("/{id}")
    public ResponseEntity<Order>  findByID(@PathVariable long id){
        try {
            return new ResponseEntity<>(orderActivityService.findById(id), HttpStatus.OK);
        } catch (NoSuchElementException ex) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);

        }
    }
    @PostMapping
    public ResponseEntity<Order> create(@RequestBody Order order) {
        return new ResponseEntity<Order>(orderActivityService.create(order), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<Order> update(@RequestBody Order order) {
        try {
            return new ResponseEntity<Order>(orderActivityService.update(order),HttpStatus.OK);
        }catch (NoSuchElementException ex){
            LOG.debug("update for unknown id: [" + order + "] - order");
            return new ResponseEntity<Order>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable long id){
        try {
            orderActivityService.delete(id);
        }catch(EmptyResultDataAccessException ex){
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }


}
