package com.training.tradingapp.repository;

import com.training.tradingapp.entities.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderActivityRepository extends JpaRepository<Order,Long> {

}
