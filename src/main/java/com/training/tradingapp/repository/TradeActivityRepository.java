package com.training.tradingapp.repository;

import com.training.tradingapp.entities.Trade;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TradeActivityRepository extends JpaRepository<Trade,Long> {
}
