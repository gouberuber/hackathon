package com.training.tradingapp.entities;

import com.training.tradingapp.enums.BuySell;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class OrderTest {
    private static final String testStockTicker = "GMC";
    private static final BuySell testBuyOrSell = BuySell.BUY;
    private Order order;

    @BeforeEach
    public void setup() {
        this.order = new Order();
    }

    @Test
    public void setCompanyNameTest() {
        this.order.setStockTicker(testStockTicker);

        assertEquals(testStockTicker, this.order.getStockTicker());
    }

    @Test
    public void setBuyOrSell() {
        this.order.setBuyOrSell(testBuyOrSell);

        assertEquals(testBuyOrSell, this.order.getBuyOrSell());
    }
}
